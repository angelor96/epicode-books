# Epicode Books

Gestione del catalogo di una biblioteca.

## Specifiche 

La libreria gestisce nel catalogo libri e periodici.

Per ogni libro � necessario gestire:
- Titolo
- Autori(possono essere pi� di uno)
- Editore
- Data di pubblicazione 
- Codice ISBN( se presente)
- Ricerca di copie a disposizione
- Volumi in cui � strutturato
- Lingua
- Categoria di classificazione(anche pi� di una)
- Numero di pagine totali( di tutti i ivolumi)

Ogni copia a disposizione ha
- Una posizione bibliotecaria
- Una indicazione che ccomunica se disponibile per il prestito o meno
- Uno starto8in consultazine, in prestito, disponibile)

Ogni volume ha:
- Titolo
- Codice ISBN( se presente)
- Codice interno
- Numero di pagine

Ogni autore � caratterizzato da 
- Nome
- Cognome
- Nazionalit�
- Anno di nascita
- Anno di morte(se non in vita)

I periodii sono caratterizzati da
- Data di pubblicazione
- Editore
- Titolo
- Categoria di classificazione:(anche pi� di una)
- Periodicit�(quotidiano, mesnsile, annuale, estemporanea, ecc)


## Permessi
Gli ooperatori della biblioteca possono gestire il catalogo, mentre gli utenti possono fare solo delle ricerche.

