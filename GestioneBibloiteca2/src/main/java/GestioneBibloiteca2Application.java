

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneBibloiteca2Application {

	public static void main(String[] args) {
		SpringApplication.run(GestioneBibloiteca2Application.class, args);
	}

}
