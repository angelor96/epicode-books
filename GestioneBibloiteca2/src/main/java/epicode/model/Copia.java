package epicode.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import epicode.model.enumType.Stato;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Copia extends BaseEntity{
	
	private String posizione;
	private boolean disponibile;
	@Enumerated(EnumType.STRING)
	private Stato stato;
	@ManyToOne
	private Volume volume;

}
