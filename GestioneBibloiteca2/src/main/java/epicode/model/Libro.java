package epicode.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Libro extends BaseEntity{
	
	private String titoloLibro;
	@ManyToOne
	private Editore editore;
	private LocalDate localDate;
	private long codiceISBN;
	private int codiceBiblioteca;
	private int numeroCopie;
	
	@ManyToOne
	private Lingua lingua;
	@ManyToMany@JoinTable(
            name="libro_categoria",
            joinColumns= @JoinColumn(name="libro_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
        )
	private Set<Categoria> categoria;
	private int numeroPagine;
	
	
	
	

}
