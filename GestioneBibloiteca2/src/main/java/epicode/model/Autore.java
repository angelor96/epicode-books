package epicode.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Autore extends BaseEntity{
	
	private String nome;
    private String cognome;
    private String nazionalità;
    private LocalDate annoNascita;
    private LocalDate annoMorte;
    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(
            name="autore_libro",
            joinColumns= @JoinColumn(name="autore_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="libro_id", referencedColumnName="id")
        )
    private Set<Libro> libri;

}
