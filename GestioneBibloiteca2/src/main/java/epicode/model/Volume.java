package epicode.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Volume extends BaseEntity{
	
	private String titolo;
    private long codiceISBN;
    private int codiceInterno;
    private int numeroPagine; 
    @ManyToOne
    private Libro libro;
	

}
