package epicode.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import epicode.model.enumType.Periodicità;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Periodico extends BaseEntity{

	private String titolo;
	private LocalDate dataDiPubblicazione;
	@ManyToMany@JoinTable(
            name="periodico_categoria",
            joinColumns= @JoinColumn(name="periodico_id", referencedColumnName="id"),
            inverseJoinColumns= @JoinColumn(name="categoria_id", referencedColumnName="id")
        )
	private Set<Categoria> categoria;
	@ManyToOne
	private Editore editore;
	@Enumerated(EnumType.STRING)
	private Periodicità periodicità;
	
}
