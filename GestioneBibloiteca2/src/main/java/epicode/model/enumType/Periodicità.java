package epicode.model.enumType;

public enum Periodicità {
	
	QUOTIDIANO, SETTIMANALE, MENSILE, TRIMESTRALE, SEMESTRALE

}
